#!/usr/bin/env bash

genisoimage -output cloud-init.img -volid cidata -joliet -rock cloud-init/

qemu-img create -f qcow2 -F qcow2 -o backing_file=../bbb-build/bbb ./disk.qcow2 20G

qemu-system-x86_64 -m 4096 -smp 8 -net nic -net user,hostfwd=tcp::2222-:22,hostfwd=tcp::8080-:80 -drive file=disk.qcow2,if=virtio -cdrom cloud-init.img