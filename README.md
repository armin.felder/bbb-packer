## THIS IS JUST A PoC FOR NOW

# requirements
- ansible
- packer

# usage
- git submodule update --init --recursive
- ansible-galaxy collection install -r ansible/roles/ebbba.bigbluebutton/requirements.yml --force
- packer init main.pkr.hcl
- packer build main.pkr.hcl

## output 
QCOW2: ./bbb-build/bbb