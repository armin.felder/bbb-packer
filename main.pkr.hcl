packer {
  required_plugins {
    qemu = {
      version = "~> 1"
      source  = "github.com/hashicorp/qemu"
    }
    ansible = {
      version = "~> 1"
      source  = "github.com/hashicorp/ansible"
    }
    sshkey = {
      version = ">= 1.1.0"
      source = "github.com/ivoronin/sshkey"
    }
  }
}

locals {
  username          = "bbb-setup"
}

data "sshkey" "install" {
  name = "initial"
}

source "qemu" "debian" {
  disk_image        = true
  use_backing_file  = false
  iso_url           = "https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img"
  iso_checksum      = "file:https://cloud-images.ubuntu.com/focal/current/SHA256SUMS"
#  iso_url           = "https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-generic-amd64.qcow2"
#  iso_checksum      = "file:https://cloud.debian.org/images/cloud/bookworm/latest/SHA512SUMS"
  output_directory  = "bbb-build"
  disable_vnc       = true
  disk_size         = "10G"
  memory            = 4000
  cd_content = {
    "meta-data" = file("meta-data.yaml")
    "user-data" = templatefile("user-data.yaml", {ssh_pub_key = data.sshkey.install.public_key })
    "network-config" = file("network_config.yaml")
  }
  cd_label          = "cidata"
  net_device        = "virtio-net"
  disk_interface    = "virtio"
  format            = "qcow2"
  accelerator       = "kvm"
  ssh_username      = local.username
  ssh_timeout       = "20m"
  ssh_private_key_file = data.sshkey.install.private_key_path
  vm_name           = "bbb"
  boot_wait    = "1s"
  shutdown_command = "sudo cloud-init clean --logs --machine-id && sudo rm /home/bbb-setup/.ssh -Rf -R && sudo shutdown -h now"
}

build {
  sources = ["source.qemu.debian"]
  provisioner "ansible" {
    playbook_file = "./ansible/playbook.yml"
    extra_arguments = [ "--scp-extra-args", "'-O'" ]
    user = local.username
  }
}